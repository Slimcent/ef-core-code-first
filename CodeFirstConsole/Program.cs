﻿using System;
using EfCodeFirstDal.EF;
using EfCodeFirstDal.Models;
using System.Data.Entity;

namespace CodeFirstConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            /*try
            {
                Database.SetInitializer(new DataInitializer());   // Dropping database if it exist, or creating a new one
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }*/
            
            Console.WriteLine("***** Fun with ADO.NET EF Code First *****\n");
            using (var context = new EfEntities())
            {
                try
                {
                    foreach (User u in context.Users)
                    {
                        Console.WriteLine(u.Name +" "+ u.Email);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            Console.ReadLine();
        }
    }
}
