﻿
namespace EfCodeFirstDal.Models
{
    public partial class Account
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int Account_Number { get; set; }

        public decimal Balance { get; set; }

        public virtual User User { get; set; }
    }
}
