﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EfCodeFirstDal.Models
{
    public partial class User
    {
        public User()
        {
            Accounts = new HashSet<Account>();
            Transactions = new HashSet<Transaction>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
