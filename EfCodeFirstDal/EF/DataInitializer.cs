﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using EfCodeFirstDal.Models;

namespace EfCodeFirstDal.EF
{
    public class DataInitializer : DropCreateDatabaseAlways<EfEntities>
    {
        protected override void Seed(EfEntities context)
        {
            base.Seed(context);
            var AddUser = new List<User>
            {
                new User {Name = "Dave", Email = "dave@gmail.com"},
                new User {Name = "Matt", Email = "matt@domain.com"},
                new User {Name = "Steve", Email = "ste@domain.com"},
                new User {Name = "Pat", Email = "pat@domain.com"},
                new User {Name = "Mon", Email = "mon@domain.com"},
            };
            context.Users.AddOrUpdate(u => new { u.Name, u.Email }, AddUser.ToArray());
            context.SaveChanges();

            var AddAccount = new List<Account>
            {
                new Account {UserId = 1, Account_Number = 0000011111, Balance = 5000.99m},
                new Account {UserId = 2, Account_Number = 0000011176, Balance = 5000.99m},
                new Account {UserId = 3, Account_Number = 0900011111, Balance = 5000.99m},
                new Account {UserId = 4, Account_Number = 0099011111, Balance = 5000.99m},
                new Account {UserId = 5, Account_Number = 0000711111, Balance = 5000.99m},
            };
            context.Accounts.AddOrUpdate(a => new { a.UserId, a.Account_Number, a.Balance }, AddAccount.ToArray());
            context.SaveChanges();
        }
}   }
